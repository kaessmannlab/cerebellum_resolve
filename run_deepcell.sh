#!/bin/bash

python deepcell-applications/run_app.py \
  mesmer \
  --output-directory ./deepcell_out \
  --output-name deepcell_mask.tif \
  --nuclear-image 32952-944-slide10_submission/32952-944-slide10_D2-1_DAPI.tiff \
  --membrane-image 32952-944-slide10_submission/32952-944-slide10_D2-1_WGA633.tiff \
  --compartment "whole-cell" \
  --image-mpp 0.138 \
  --squeeze

