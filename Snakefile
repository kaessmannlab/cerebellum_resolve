# author: Kevin Leiss

# note:
# the following pipeline starts AFTER the Baysor run, since
# the installation of this tool was cumbersome and is not portable


# preprocessing
# Takes the Baysor output and converts it to a sparse matrix
# additionally, some filtering for high segmentation confidence
# is applied (second argument).
rule preprocess:
  input:
    "baysor_output_32952_{sample}/segmentation_cell_stats.csv",
    "baysor_output_32952_{sample}/segmentation_counts.tsv"
  output:
    "results/{sample}/sparse_counts/barcodes.tsv",
    "results/{sample}/sparse_counts/features.tsv",
    "results/{sample}/sparse_counts/matrix.mtx",
    "results/{sample}/metadata.csv"
  threads: 1
  conda: "env.yaml"
  shell:
    '''
    Rscript --vanilla \
    scripts/preprocess.R \
      baysor_output_32952_{wildcards.sample} \
      0.95 \
      results/{wildcards.sample}
    '''

# create the single cell andata objects
# first step extracts the relevant data from the
# SCE R objects, and then uses these tables
# to create the AnData scanpy object.

rule create_flatfiles:
  input: 
    sce = "data/sce_objects/{species}_sce.rds",
    script = "scripts/create_flatfiles_from_sce.R"
  output:
    "single_nuc_data/{species}/dimred_umap2d.csv",
    "single_nuc_data/{species}/dimred_liger.csv",
    "single_nuc_data/{species}/metadata.csv",
    "single_nuc_data/{species}/sparse_matrix/barcodes.tsv",
    "single_nuc_data/{species}/sparse_matrix/features.tsv",
    "single_nuc_data/{species}/sparse_matrix/matrix.mtx"
  conda: "env.yaml"
  shell: 
    '''
    Rscript --vanilla \
      scripts/create_flatfiles_from_sce.R \
      {input.sce} \
      single_nuc_data/{wildcards.species}/
    '''

# and here comes step 2:
rule create_andata_from_flatfiles:
  input:
    "single_nuc_data/{species}/dimred_umap2d.csv",
    "single_nuc_data/{species}/dimred_liger.csv",
    "single_nuc_data/{species}/metadata.csv",
    "single_nuc_data/{species}/sparse_matrix/barcodes.tsv",
    "single_nuc_data/{species}/sparse_matrix/features.tsv",
    "single_nuc_data/{species}/sparse_matrix/matrix.mtx",
    "scripts/create_andata_from_sce.py",
    "scripts/resolve.py"
  output:
    "single_nuc_data/{species}_andata.h5"
  conda: "env.yaml"
  threads: 1
  shell:
    '''
    python scripts/create_andata_from_sce.py \
      single_nuc_data/{wildcards.species} \
      {output}
    '''

# integrate the resolve data with our single nucleus data
# uses tangram to do the projection
rule integration_slide10:
  input:
    "results/slide10/sparse_counts/barcodes.tsv",
    "results/slide10/sparse_counts/features.tsv",
    "results/slide10/sparse_counts/matrix.mtx",
    "results/slide10/metadata.csv",
    "single_nuc_data/hum_andata.h5"
  output:
    "results/slide10/hum_integration/single_cell_map.h5",
    "results/slide10/hum_integration/hum_sc_adata.h5",
    "results/slide10/hum_integration/spacial_adata.h5",
  threads: 1
  conda: "env.yaml"
  shell:
    '''
    mkdir -p results/slide10/hum_integration
    
    python \
      scripts/integration.py \
      results/slide10 \
      "11 wpc" \
      single_nuc_data/hum_andata.h5 \
      results/slide10/hum_integration
    '''
    
# integrate the resolve data with our single nucleus data
# uses tangram to do the projection
rule integration_slide10_mou:
  input:
    "results/slide10/sparse_counts/barcodes.tsv",
    "results/slide10/sparse_counts/features.tsv",
    "results/slide10/sparse_counts/matrix.mtx",
    "results/slide10/metadata.csv",
    "single_nuc_data/mou_andata.h5"
  output:
    "results/slide10/mou_integration/single_cell_map.h5",
    "results/slide10/mou_integration/mou_sc_adata.h5",
    "results/slide10/mou_integration/spacial_adata.h5",
  threads: 1
  conda: "env.yaml"
  shell:
    '''
    mkdir -p results/slide10/mou_integration
    python \
      scripts/integration_mou.py \
      results/slide10 \
      "E15.5" \
      single_nuc_data/mou_andata.h5 \
      results/slide10/mou_integration
    '''

rule integration_slide3A1:
  input:
    "results/slide3A1/sparse_counts/barcodes.tsv",
    "results/slide3A1/sparse_counts/features.tsv",
    "results/slide3A1/sparse_counts/matrix.mtx",
    "results/slide3A1/metadata.csv",
    "single_nuc_data/hum_andata.h5"
  output:
    "results/slide3A1/single_cell_map.h5",
    "results/slide3A1/hum_sc_adata.h5",
    "results/slide3A1/spacial_adata.h5",
  threads: 1
  conda: "env.yaml"
  shell:
    '''
    python \
      scripts/integration.py \
      results/slide3A1 \
      "9 wpc" \
      single_nuc_data/hum_andata.h5
    '''

rule integration_slide3D1:
  input:
    "results/slide3D1/sparse_counts/barcodes.tsv",
    "results/slide3D1/sparse_counts/features.tsv",
    "results/slide3D1/sparse_counts/matrix.mtx",
    "results/slide3D1/metadata.csv",
    "single_nuc_data/hum_andata.h5"
  output:
    "results/slide3D1/single_cell_map.h5",
    "results/slide3D1/hum_sc_adata.h5",
    "results/slide3D1/spacial_adata.h5",
  threads: 1
  conda: "env.yaml"
  shell:
    '''
    python \
      scripts/integration.py \
      results/slide3D1 \
      "9 wpc" \
      single_nuc_data/hum_andata.h5
    '''

# projecting the metadata
rule project:
  input:
    "results/{sample}/hum_integration/single_cell_map.h5",
    "results/{sample}/hum_integration/spacial_adata.h5"
  output:
    "results/{sample}/hum_integration/tangram_{proj_annotation}_prob.csv",
    "results/{sample}/hum_integration/tangram_{proj_annotation}_probMax.csv"
  threads: 1
  conda: "env.yaml"
  shell:
    '''
    python \
      scripts/project_metadata.py \
      results/{wildcards.sample}/hum_integration \
      {wildcards.proj_annotation}
    '''

# projecting the metadata
rule project_mou:
  input:
    "results/{sample}/mou_integration/single_cell_map.h5",
    "results/{sample}/mou_integration/spacial_adata.h5"
  output:
    "results/{sample}/mou_integration/tangram_{proj_annotation}_prob.csv",
    "results/{sample}/mou_integration/tangram_{proj_annotation}_probMax.csv"
  threads: 1
  conda: "env.yaml"
  shell:
    '''
    python \
      scripts/project_metadata.py \
      results/{wildcards.sample}/mou_integration \
      {wildcards.proj_annotation}
    '''


# plotting the projection on the spacial data
# as well as generating boxplots for all spacial genes
# (genes beeing detected in the resolve experiment)
rule plot_projection:
  input:
    "results/{sample}/{species}_integration/tangram_{proj_annotation}_probMax.csv",
    "metadata/color_codes_annotation.csv",
    "results/{sample}/sparse_counts/barcodes.tsv",
    "results/{sample}/sparse_counts/features.tsv",
    "results/{sample}/sparse_counts/matrix.mtx"
  output:
    "results/{sample}/{species}_integration/spacial_sce_{proj_annotation}.rds",
    "results/{sample}/{species}_integration/plots/{proj_annotation}/projected_annotation.pdf",
    directory("results/{sample}/{species}_integration/plots/{proj_annotation}/resolve_gene_plots")
  threads: 1
  conda: "env.yaml"
  shell:
    '''
    Rscript --vanilla \
      scripts/plot_projection.R \
      results/{wildcards.sample}/{wildcards.species}_integration/ \
      {wildcards.proj_annotation} \
      results/{wildcards.sample}/sparse_counts
    '''


# plotting the spacial gene expression values on
# the 2D grid
rule plot_spacial_genes:
  input:
    "results/{sample}/metadata.csv",
    "results/{sample}/sparse_counts/barcodes.tsv",
    "results/{sample}/sparse_counts/features.tsv",
    "results/{sample}/sparse_counts/matrix.mtx"
  output:
    directory("results/{sample}/plots/spacial_genes")
  threads: 1
  conda: "env.yaml"
  shell:
    '''
    Rscript --vanilla \
      scripts/plot_spacial_genes.R \
      results/{wildcards.sample}
    '''

