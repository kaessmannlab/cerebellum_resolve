# Cerebellum Resolve analysis

## Introduction
Details about the processing can be found in our paper (link will be added here, when ready).

Input data can be provided upon request.

## Analysis
The resolve data was segmented using [Baysor](https://github.com/kharchenkolab/Baysor) version 0.5.0.
settings for the run can be found in the [config.toml](,/config.toml) file.
Each resolve run was processed separately.

Subsequent analyses are documented in the [Snakefile](./Snakefile).
