import scanpy as sc
import pandas as pd
import tangram as tg
import numpy as np

import logging
import sys

args = sys.argv

preprocess_out = args[1]
proj_annotation = args[2]


logging.info('Reading the data')
# reading the data
ad_map = sc.read_h5ad(f'{preprocess_out}/single_cell_map.h5')
sp = sc.read_h5ad(f'{preprocess_out}/spacial_adata.h5')

logging.info('projecting the annotation')
logging.info(f'projecting: {proj_annotation}')
# projecting the annotation
tg.project_cell_annotations(ad_map, sp, annotation=proj_annotation)

logging.info('Saving the projection metadata')
sp.obs[f'pred_{proj_annotation}'] = sp.obsm['tangram_ct_pred'].apply(lambda x: np.array(list(x.index))[x == x.max()][0], axis=1)


logging.info('writing the output')
sp.obsm['tangram_ct_pred'].to_csv(f'{preprocess_out}/tangram_{proj_annotation}_prob.csv')
sp.obs.to_csv(f'{preprocess_out}/tangram_{proj_annotation}_probMax.csv')
