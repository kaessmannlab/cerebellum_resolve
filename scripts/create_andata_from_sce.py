#!/usr/bin/env python

import logging
import resolve
import sys

logging.basicConfig(format='[%(asctime)s] - %(funcName)s: %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


def main() -> None:

    args = sys.argv

    logger.info("Reading data from:")
    flat_data_folder = args[1]
    logger.info(flat_data_folder)

    logger.info("Saving output to:")
    outfile = args[2]
    logger.info(outfile)

    adata = resolve.read_flat_data_to_andata(mm = f"{flat_data_folder}/sparse_matrix/matrix.mtx",
                                             obs_names = f"{flat_data_folder}/sparse_matrix/barcodes.tsv",
                                             var_names = f"{flat_data_folder}/sparse_matrix/features.tsv",
                                             obs = f"{flat_data_folder}/metadata.csv",
                                             obsm = {'umap': f"{flat_data_folder}/dimred_umap2d.csv",
                                                     'liger': f"{flat_data_folder}/dimred_liger.csv"})
    adata.write_h5ad(outfile)



if __name__ == "__main__":
    main()

# hum = rs.read_flat_data_to_andata( 'single_nuc_data/sparse_matrix/matrix.mtx',
#                                 'single_nuc_data/sparse_matrix/barcodes.tsv',
#                                 'single_nuc_data/sparse_matrix/features.tsv',
#                                 'single_nuc_data/metadata.csv',
#                                 obsm={'umap': 'single_nuc_data/dimred_umap2d.csv',
#                                       'liger': 'single_nuc_data/dimred_liger.csv'} )

# hum.var.index.name = 'gene_id'

# hum.write_h5ad('single_nuc_data/hum_andata.h5')
