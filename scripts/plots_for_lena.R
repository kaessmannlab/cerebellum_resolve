library(tidyverse)
library(SingleCellExperiment)

slide.names <- c(
                 'slide3A1',
                 'slide3D1',
                 'slide10'
)
names(slide.names) <- slide.names

annot.names <- c('cell_type',
                 'dev_state',
                 'subtype')
names(annot.names) <- annot.names


# reading in the cell type colors
cls <- read_csv('../cerebellum_analysis/data/metadata/color_codes_annotation.csv')

cell_type_color_df <- cls %>%
  select(cell_type,
         cell_type_colour) %>%
  distinct()
cell_type_color <- cell_type_color_df$cell_type_colour
names(cell_type_color) <- cell_type_color_df$cell_type

use_devstate <- cls %>%
  select(cell_type, dev_state, subtype) %>%
  distinct() %>%
  filter(cell_type %in% c('GC','UBC', 'GC/UBC') |
         subtype == 'progenitor_RL') %>%
  select(dev_state) %>%
  distinct() %>%
  pull('dev_state')
use_devstate <- use_devstate[!is.na(use_devstate)]
use_devstate <- c(use_devstate,
                  'other')

use_devstate_color_df <- cls %>%
  filter(dev_state %in% use_devstate) %>%
  select(dev_state, dev_state_colour) %>%
  distinct()
use_devstate_color <- use_devstate_color_df$dev_state_colour
names(use_devstate_color) <- use_devstate_color_df$dev_state
use_devstate_color <- c(use_devstate_color,
                        c('other' = 'gray'))

use_subtype <- cls %>%
  select(cell_type, dev_state, subtype) %>%
  distinct() %>%
  filter(cell_type %in% c('GC','UBC', 'GC/UBC') |
         subtype == 'progenitor_RL') %>%
  select(subtype) %>%
  distinct() %>%
  pull('subtype')
use_subtype <- use_subtype[!is.na(use_subtype)]
use_subtype <- c(use_subtype,
                 'other')

use_subtype_color_df <- cls %>%
  filter(subtype %in% use_subtype) %>%
  select(subtype, subtype_colour) %>%
  distinct()
use_subtype_color <- use_subtype_color_df$subtype_colour
names(use_subtype_color) <- use_subtype_color_df$subtype
use_subtype_color <- c(use_subtype_color,
                       c('other' = 'gray'))

slides <- lapply(slide.names,
                 function(sn){
                   lapply(annot.names,
                          function(annot){
                            readRDS(glue::glue("results/{sn}/spacial_sce_{annot}.rds"))
                          })
                 })


# sections with all cell_type labels
theme_set(theme_classic())

dir.create('plots/for_lena', showWarnings = FALSE, recursive = TRUE)

for(slide.name in names(slides)){
  slide <- slides[[slide.name]]$cell_type
  df <- colData(slide) %>%
    as.data.frame() %>%
    as_tibble()

  plt <- df %>%
    ggplot(aes(x=x,y=y,
               color = pred_cell_type)) +
                 geom_point(size = 0.01) +
                 coord_fixed() +
                 scale_color_manual(values = cell_type_color) +
                 guides(color = guide_legend(override.aes = list(size = 2)))
   ggsave(glue::glue('plots/for_lena/{slide.name}_celltypes.pdf'),
          width = 10, height = 6)


   slide <- slides[[slide.name]]$dev_state
   df <- colData(slide) %>%
     as.data.frame() %>%
     as_tibble()

   plt <- df %>%
     mutate(pred_dev_state = ifelse(pred_dev_state %in% use_devstate,
                                    pred_dev_state,
                                    'other')) %>%
     ggplot(aes(x=x,y=y,
                color=pred_dev_state)) +
     geom_point(size=0.01) +
     coord_fixed() +
     scale_color_manual(values = use_devstate_color) +
     guides(color = guide_legend(override.aes = list(size = 2)))
   ggsave(glue::glue('plots/for_lena/{slide.name}_devstate.pdf'),
          width=10, height=6)


   slide <- slides[[slide.name]]$subtype
   df <- colData(slide) %>%
     as.data.frame() %>%
     as_tibble()

   plt <- df %>%
     mutate(pred_subtype = ifelse(pred_subtype %in% use_subtype,
                                    pred_subtype,
                                    'other')) %>%
     ggplot(aes(x=x,y=y,
                color=pred_subtype)) +
     geom_point(size=0.01) +
     coord_fixed() +
     scale_color_manual(values = use_subtype_color) +
     guides(color = guide_legend(override.aes = list(size = 2)))
   ggsave(glue::glue('plots/for_lena/{slide.name}_subtype.pdf'),
          width=10, height = 6)
}
