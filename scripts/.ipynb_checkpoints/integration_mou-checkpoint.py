import sys
import numpy as np
import pandas as pd
import scanpy as sc
import tangram as tg

# custom module
import resolve as rs
import seaborn as sns


args = sys.argv



base_input_dir = args[1]
use_stage = args[2]
andata_path = args[3]
output_dir = args[4]


# reading in the mouan data
mou = sc.read_h5ad(andata_path)
mou.var.index.name = "gene_id"

mou = mou[ mou.obs['Stage'] == use_stage, :]
sc.pp.filter_genes(mou, min_cells = 20)

# preparing the segmentation data
sp = sc.read_mtx(f'{base_input_dir}/sparse_counts/matrix.mtx').T

obs = pd.read_csv(f'{base_input_dir}/sparse_counts/barcodes.tsv',
                  header=None,
                  index_col=None)
obs.columns = ['cell_id']
obs = obs.set_index('cell_id')

obs_metadata = pd.read_csv(f'{base_input_dir}/metadata.csv')
obs_metadata = obs_metadata.reset_index().set_index('cell_id')

var = pd.read_csv(f'{base_input_dir}/sparse_counts/features.tsv',
                header=None,
                index_col=None)
var.columns = ['gene_name']
var = var.set_index('gene_name')

sp.var = var
sp.obs = obs

sp.obs = sp.obs.join(obs_metadata)

# gene name translation
gns_all = pd.read_csv('metadata/all_genes_meta.csv')

gns_all = gns_all.loc[ gns_all['species'] == 'MOU', : ]

gns_trans = gns_all[['gene_id', 'gene_name']].drop_duplicates()

tmp = mou.var.copy()
tmp.index.name = 'gene_id'
tmp = tmp.join(gns_trans.set_index('gene_id'))
gene_translate = {tmp.index.values[i]: tmp['gene_name'].values[i] for i in range(0, tmp.shape[0])}
translated_ids = mou.var.reset_index()['gene_id'].map(lambda x: gene_translate[x])
translated_ids.index = mou.var.index.values

# renaming the mou gene IDs to gene symbols
mou.var['gene_name'] = translated_ids.str.upper()
mou.var = mou.var.reset_index().set_index('gene_name')


# copying the raw data
mou.raw = mou.copy()
sp.raw = sp.copy()

sc.pp.normalize_total(mou)
sc.pp.normalize_total(sp)

# integration

mou.write_h5ad(f'{output_dir}/sc_adata.h5')
sp.write_h5ad(f'{output_dir}/spacial_adata.h5')

tg.pp_adatas(mou, sp)
ad_map = tg.map_cells_to_space(mou, sp)

# saving the integration matrix
ad_map.write_h5ad(f'{output_dir}/single_cell_map.h5')

# projeciting the dev_state
# tg.project_cell_annotations(ad_map, sp, annotation='dev_state')

# projecting gene expression matrix
sp_proj_genes = tg.project_genes(ad_map, mou)
sp_proj_genes.write_h5ad(f'{output_dir}/spacial_with_projected_genes.h5')
sp_proj_genes.obs.to_csv(f'{output_dir}/spacial_with_projected_genes_obs.csv')
sp_proj_genes.var.to_csv(f'{output_dir}/spacial_with_projected_genes_var.csv')
