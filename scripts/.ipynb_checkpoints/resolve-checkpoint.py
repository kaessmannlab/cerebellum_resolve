import pandas as pd
import numpy as np
import scanpy as sc

# preparing the logging
import logging
logging.basicConfig(format='[%(asctime)s] - %(funcName)s: %(message)s')
logger = logging.getLogger(__name__)

def read_flat_data_to_andata( mm, obs_names, var_names, obs = None,
    obsm = None):

    logger.info('Reading MM file')
    adata = sc.read_mtx( mm ).T

    logger.debug(adata)

    logger.info('Reading in row and column data')
    rd = pd.read_csv(obs_names,
                 header=None,
                 index_col=0,
                      sep = '\t')
    rd.index.name = 'cell_id'

    cd = pd.read_table(var_names,
                     header=None,
                     index_col=0,
                      sep = '\t')
    cd.index.name = 'gene_name'

    if obs is not None:
        md = pd.read_csv(obs, index_col=0, header=0)
        logger.debug(md.head())


        rd = md.join( rd )

    logger.debug(rd.head())
    logger.debug(cd.head())

    adata.obs = rd
    adata.var = cd

    if obsm is not None:
        logger.info('Reading the dimensional reduction data')

        for key, value in obsm.items():
            tmp = pd.read_csv(value, header=0, index_col=0)
            adata.obsm[key] = tmp.values

    return adata.copy()

