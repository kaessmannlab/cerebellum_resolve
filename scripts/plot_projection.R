suppressPackageStartupMessages({
  library(tidyverse)
  library(Matrix)
  library(SingleCellExperiment)
})

theme_set(theme_classic())

# parsing the commandline arguments
args <- commandArgs(trailingOnly = TRUE)

# output folder of preoject_metadata.py
integrated_folder <- args[1]

# e.g. dev_state
projected_mdata <- args[2]

sparse_counts_folder <- args[3]

md <- read_csv(file.path(
  integrated_folder,
  glue::glue("tangram_{projected_mdata}_probMax.csv")
))


# preparing the color scheme ---------------------------------------------------

ctc <- read_csv("metadata/color_codes_annotation.csv")
ctc <- ctc %>%
  select(
    origin, cell_type, dev_state, cell_type_colour, dev_state_colour,
    subtype, subtype_colour
  )

ctc_list <- lapply(
  c(
    "dev_state" = "dev_state",
    "cell_type" = "cell_type",
    "subtype" = "subtype"
  ),
  function(x) {
    tmp <- ctc %>%
      filter(!!sym(x) != "NA") %>%
      filter(!!sym(x) %in% unique(md %>% pull(paste0("pred_", projected_mdata)))) %>%
      distinct()


    tmp2 <- tmp %>%
      select(all_of(c(x, paste0(x, "_colour")))) %>%
      distinct()


    out <- tmp2 %>% pull(paste0(x, "_colour"))
    names(out) <- tmp2 %>% pull(x)
    out <- out[order(names(out))]
    return(out)
  }
)
# ------------------------------------------------------------------------------



if (!file.exists(file.path(integrated_folder, glue::glue("spacial_sce_{projected_mdata}.rds")))) {
  mm <- readMM(file.path(sparse_counts_folder, "matrix.mtx"))
  mm <- as(mm, "dgCMatrix")

  cdata <- read_delim(file.path(sparse_counts_folder, "barcodes.tsv"),
    delim = "\t",
    col_names = FALSE
  )

  rdata <- read_delim(file.path(sparse_counts_folder, "features.tsv"),
    delim = "\t",
    col_names = FALSE
  )


  rownames(mm) <- rdata %>% pull(1)
  colnames(mm) <- cdata %>% pull(1)

  cdata <- md %>%
    as.data.frame() %>%
    column_to_rownames("cell_id")

  sce <- SingleCellExperiment(list(counts = mm),
    colData = cdata
  )

  saveRDS(sce, file = file.path(integrated_folder, glue::glue("spacial_sce_{projected_mdata}.rds")))
} else {
  sce <- readRDS(file.path(integrated_folder, glue::glue("spacial_sce_{projected_mdata}.rds")))
}

outfolder <- file.path(integrated_folder, "plots", projected_mdata, "resolve_gene_plots")
dir.create(outfolder, showWarnings = FALSE, recursive = TRUE)

# plotting the projected annotation
colnames(md)[colnames(md) == paste0("pred_", projected_mdata)] <- "annot"
plt <- md %>%
  ggplot(aes(x = x, y = y, color = annot)) +
  geom_point(size = 0.01) +
  guides(color = guide_legend(override.aes = list(size = 2))) +
  coord_fixed()

if (projected_mdata %in% names(ctc_list)) {
  plt <- plt +
    scale_color_manual(values = ctc_list[[projected_mdata]])
}

ggsave(file.path(
  integrated_folder,
  "plots",
  projected_mdata,
  "projected_annotation.pdf"
),
plot = plt,
width = 12,
height = 12,
useDingbats = FALSE
)


# plotting the detected spacial genes
nrm <- counts(sce)
nrm <- t(nrm) / colSums(nrm)
md <- colData(sce) %>%
  as.data.frame() %>%
  rownames_to_column("cell_id")
colnames(md)[colnames(md) == paste0("pred_", projected_mdata)] <- "annot"
for (g in rownames(sce)) {
  plt <- md %>%
    mutate(annot = factor(annot, levels = rev(unique(sort(annot))))) %>%
    ggplot(aes(
      x = annot, y = nrm[, g],
      fill = annot
    )) +
    geom_boxplot(outlier.size = 0.1) +
    coord_flip() +
    theme(legend.position = "none")

  if (projected_mdata %in% names(ctc_list)) {
    plt <- plt +
      scale_fill_manual(values = ctc_list[[projected_mdata]])
  }

  ggsave(file.path(outfolder, glue::glue("z_{g}.pdf")),
    width = 4, height = 7
  )
}
